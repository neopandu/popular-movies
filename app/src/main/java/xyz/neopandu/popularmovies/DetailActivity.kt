package xyz.neopandu.popularmovies

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.content_detail.*
import xyz.neopandu.popularmovies.model.Movie

class DetailActivity : AppCompatActivity() {
    private fun getDetailUrl(movieId: Int): String {
        val apiKey = getString(R.string.api_key)
        return "https://api.themoviedb.org/3/movie/$movieId?api_key=$apiKey&language=en-US"
    }

    private val movie by lazy { intent?.extras?.getParcelable<Movie>("movieModel") }


    private val imgBackdropBaseUrl = "https://image.tmdb.org/t/p/w500"
    private val imgPosterBaseUrl = "https://image.tmdb.org/t/p/w154"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = movie?.title ?: "N/A"

        val backdropUrl = movie?.bannerUrl
        backdropUrl?.let{
            Glide.with(this).load(imgBackdropBaseUrl + it).into(img_detail_backdrop)
        }

        val posterUrl = movie?.posterUrl
        posterUrl?.let {
            Glide.with(this).load(imgPosterBaseUrl + it).into(img_detail_poster)
        }

        tv_detail_title.text = movie?.title
        tv_detail_score.text = movie?.vote.toString()
        tv_detail_overview.text = movie?.description
        tv_detail_release.text = movie?.releaseDate
    }
}
