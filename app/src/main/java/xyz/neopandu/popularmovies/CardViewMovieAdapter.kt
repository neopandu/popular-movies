package xyz.neopandu.popularmovies

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import xyz.neopandu.popularmovies.model.Movie


class CardViewMovieAdapter(private val context: Context) :
    RecyclerView.Adapter<CardViewMovieAdapter.CardViewViewHolder>() {
    private var movies: MutableList<Movie> = mutableListOf()

    private val imgBaseUrl = "https://image.tmdb.org/t/p/w154"

    fun updateMovies(newMovieList: List<Movie>) {
        movies.clear()
        movies.addAll(newMovieList)
        notifyDataSetChanged()
    }
    fun addMovies(newMovieList: List<Movie>) {
        movies.addAll(newMovieList)
        notifyDataSetChanged()
    }

    private var onItemClick : ((Movie) -> Unit)? = null

    fun setOnItemClick(onItemClick : ((Movie) -> Unit)) {
        this.onItemClick = onItemClick
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CardViewViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_row_movie, viewGroup, false)
        return CardViewViewHolder(view)
    }

    override fun onBindViewHolder(cardViewViewHolder: CardViewViewHolder, i: Int) {
        val movie = movies[i]
        cardViewViewHolder.bind(movie)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    inner class CardViewViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val imgPoster: ImageView = itemView.findViewById(R.id.img_item_poster)
        private val tvTitle: TextView = itemView.findViewById(R.id.tv_item_title)
        private val tvOverview: TextView = itemView.findViewById(R.id.tv_item_overview)
        private val tvVote: TextView = itemView.findViewById(R.id.tv_item_vote)

        fun bind(movie: Movie) {
            Glide.with(context).load(imgBaseUrl + movie.posterUrl).into(imgPoster)
            tvTitle.text = movie.title
            tvOverview.text = movie.description
            tvVote.text = movie.vote.toString()

            itemView.isClickable = true
            itemView.setOnClickListener {
                onItemClick?.invoke(movie)
            }
        }
    }
}