package xyz.neopandu.popularmovies

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import xyz.neopandu.popularmovies.model.Movie

class MainActivity : AppCompatActivity() {

    private val moviesAdapter by lazy { CardViewMovieAdapter(this) }

    private val apiKey = getString(R.string.api_key)
    private val url =
        "https://api.themoviedb.org/3/movie/popular?api_key=$apiKey&language=en-US&page="

    private val queue by lazy { Volley.newRequestQueue(this) }
    private var page = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv_movies.layoutManager = LinearLayoutManager(this)
        rv_movies.adapter = moviesAdapter

        rv_movies.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = recyclerView.layoutManager?.childCount ?: 0
                val totalItemCount = recyclerView.layoutManager?.itemCount ?: 0
                val firstVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager?)?.findFirstVisibleItemPosition() ?: 0

                if (!swipe_refresh.isRefreshing) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                        page += 1
                        fetchMovies()
                    }
                }
            }
        })

        moviesAdapter.setOnItemClick { movie ->
            startActivity(Intent(this, DetailActivity::class.java).putExtra("movieModel", movie))
        }

        swipe_refresh.setOnRefreshListener {
            page = 1
            fetchMovies()
        }

        fetchMovies()
    }

    private fun fetchMovies() {
        swipe_refresh.isRefreshing = true
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url + page.toString(), null,
            Response.Listener { response ->
                val objMovies = response.getJSONArray("results")
                val count = objMovies.length()

                val movies = mutableListOf<Movie>()

                for (i in 0 until count) {
                    val objMovie = objMovies.getJSONObject(i)
                    movies.add(
                        Movie(
                            id = objMovie.getInt("id"),
                            posterUrl = objMovie.getString("poster_path"),
                            bannerUrl = objMovie.getString("backdrop_path"),
                            title = objMovie.getString("title"),
                            description = objMovie.getString("overview"),
                            vote = objMovie.getDouble("vote_average"),
                            releaseDate = objMovie.getString("release_date")
                        )
                    )
                }
                if (page == 1) moviesAdapter.updateMovies(movies)
                else moviesAdapter.addMovies(movies)
                swipe_refresh.isRefreshing = false
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, error.localizedMessage, Toast.LENGTH_LONG).show()
                swipe_refresh.isRefreshing = false
                if (page > 1) page -= 1
            }
        )

        queue.add(jsonObjectRequest)
    }
}
