package xyz.neopandu.popularmovies.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class Movie(
    val id: Int,
    val title: String,
    val description: String,
    val posterUrl: String,
    val bannerUrl: String,
    val vote: Double,
    val releaseDate: String
) : Parcelable